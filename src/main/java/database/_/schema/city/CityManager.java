package database._.schema.city;

import database._.schema.city.generated.GeneratedCityManager;

/**
 * The main interface for the manager of every {@link
 * database._.schema.city.City} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface CityManager extends GeneratedCityManager {}
package database._.schema.city;

import database._.schema.city.generated.GeneratedCity;

/**
 * The main interface for entities of the {@code city}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface City extends GeneratedCity {}
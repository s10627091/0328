package database._.schema.city;

import database._.schema.city.generated.GeneratedCityImpl;

/**
 * The default implementation of the {@link
 * database._.schema.city.City}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class CityImpl 
extends GeneratedCityImpl 
implements City {}
package database._.schema.member;

import database._.schema.member.generated.GeneratedMemberSqlAdapter;

/**
 * The SqlAdapter for every {@link database._.schema.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class MemberSqlAdapter extends GeneratedMemberSqlAdapter {}